# -*- coding: utf-8 -*-
from scrapy.selector import HtmlXPathSelector
from scrapy.http import Request
from scrapy.spider import BaseSpider
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor

from scrapy.contrib.loader import ItemLoader
from huge.items import ParseItem

import re
import json


class BlizkoSpider(CrawlSpider):
    name = 'blizko'
    allowed_domains = ['blizko.ru']
    start_urls = ['http://ekb.blizko.ru',]

    packet = {
        'cl-stars': [u'Пробный'],
        'cl-stars-1': [u'Платный'],
        'cl-stars-2': [u'Серебряный'],
        'cl-stars-3': [u'Золотой'],
        'cl-stars-4': [u'Платиновый']
    }

    rules = (
        Rule(SgmlLinkExtractor(allow=('ekb.blizko.ru/gde_.+', 'ekb.blizko.ru/naiti/.+'), 
            deny=('ekb.blizko.ru/gde_.+/', 'ekb.blizko.ru/naiti/.+/', '=')), callback='parse_categories'),
    )

    def parse_end(self, response):
        hxs = HtmlXPathSelector(response)
        sites = hxs.select('//li[@class="link_site"]/a/text()').extract() or [u'Нет']
        phones = hxs.select('//li[@class="tel"]/text()').extract() or [u'Нет']
        l = ItemLoader(ParseItem())
        l.add_value('title', response.meta['title'])
        l.add_value('url', response.meta['url'])
        l.add_value('desc', response.meta['desc'])
        l.add_value('stars', response.meta['packet'])
        l.add_value('category', response.meta['category'])
        l.add_value('page', response.meta['page'])
        l.add_value('site', sites)
        l.add_value('phones', phones)
        return l.load_item()

    def parse_nextpage(self, response):
        hxs = HtmlXPathSelector(response)
        category = response.meta['category']
        for tag in hxs.select('//*[contains(@class, "js-company-box")]/div/div[contains(@class, "cli-center")]'):
            _title_box = tag.select('a') or tag.select('span')
            company_url = _title_box.select('@href').extract() or _title_box.select('@data-to').extract()
            request = Request( '%s/?intruder_3cd5eb86aae6d8e5821dbbe63f4cc302=1' %company_url[0], callback=self.parse_end)
            request.meta['title'] = _title_box.select('text()').extract()
            request.meta['url'] = company_url
            request.meta['desc'] = tag.select('p/text()').extract() or [u'Нет']
            _stars = tag.select('div/div/@class').extract()
            request.meta['packet'] = _stars and self.packet.get(_stars.pop().split(' ').pop()) or [u'Нет']
            request.meta['category'] = category
            cur_page = hxs.select('//*[@id="paginate-container-Company"]/@data-current-page').extract()
            request.meta['page'] = len(cur_page) > 0 and cur_page or ['1']
            yield request

        if 'page' not in response.url:
            pages = hxs.select('//*[@id="paginate-container-Company"]/@data-pages').extract()
            pages = len(pages) > 0 and range(1, int(pages.pop())+1) or [1]
            for page in pages:
                request = Request('%s?page=%s' %(response.url, page), callback=self.parse_nextpage)
                request.meta['category'] = category 
                yield request                       

    def parse_categories(self, response):
        hxs = HtmlXPathSelector(response)   
        for tag in hxs.select('//*[@class="lm-lvl-2-link"]'):
            request = Request(tag.select('@href').extract().pop(), callback=self.parse_nextpage)
            request.meta['category'] = tag.select('text()').extract()
            yield request


class PulscenCrawl(CrawlSpider):
    name = 'pulscen'
    top_categories = {}

    allowed_domains = ['pulscen.ru']
    start_urls = ['http://ekb.pulscen.ru/firms',]

    packet = {
        '1': u'Серебряный',
        '2': u'Золотой',
        '3': u'Платиновый',
        '4': u'Платиновый+'
    }   

    rules = (
            Rule(SgmlLinkExtractor(allow=('ekb.pulscen.ru/firms/[0-9]{4}-.+',), 
                deny=('recent'), tags='a'), follow=True, callback='parse_nextpage'),
    )

    def parse_start_url(self, response):
        hxs = HtmlXPathSelector(response)
        for tag in hxs.select('//*[@class="sub-rubrics"]/a'):
            self.top_categories.update({re.search('\d+', tag.select('@href')
                .extract().pop()).group(): tag.select('text()').extract().pop()})

    def parse_end(self, response):      
        hxs = HtmlXPathSelector(response)
        l = ItemLoader(ParseItem())
        l.add_value('url', response.url)
        l.add_value('page', response.meta['page'])
        l.add_value('category', self.top_categories.get(response.meta['category']))
        l.add_value('site', hxs.select('//*[@class="js-site-url-link"]/text()').extract() or u'Нет')
        l.add_value('stars', response.meta['stars'])
        l.add_value('phones', hxs.select('//*[@class="js-tel"]/text()').extract() or u'Нет')
        l.add_value('title', response.meta['title'])
        l.add_value('desc', response.meta['desc'])
        yield l.load_item()

    def parse_nextpage(self, response):
        hxs = HtmlXPathSelector(response)       
        for tag in hxs.select('//*[contains(@class, "bp-content")]'):
            request = Request('%s/?intruder_1dcca23355272056f04fe8bf20edfce0=2' %tag.select('ul/li/span[@data-to]/@data-to').extract().pop(),
                  callback=self.parse_end)
            cur_page = hxs.select('//*[@id="paginate-container-company"]/@data-current-page').extract()
            request.meta['category'] = re.search('\d+', response.url).group()
            request.meta['page'] = len(cur_page) > 0 and cur_page or ['1']
            request.meta['title'] = tag.select('ul/li/span[@data-to]/text()').extract()
            request.meta['desc'] = tag.select('p/text()').extract() or u'Нет'
            request.meta['stars'] = tag.select('ul/li/i/@data-packet').extract() or u'Нет'
            yield request

        if 'page' not in response.url:
            pages = hxs.select('//*[@id="paginate-container-company"]/@data-pages').extract()
            pages = len(pages) > 0 and range(1, int(pages.pop())+1) or [1]
            for page in pages:
                yield Request('%s?page=%s' %(response.url, page), callback=self.parse_nextpage)