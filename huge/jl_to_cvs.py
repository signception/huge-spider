# coding: utf-8
import sys
import json


f = sys.argv[1]
file_stream = open(f).readlines()
file_cvs = open('%s.csv' %f.split('.')[0], 'wb')
append_header = False

for line in file_stream:
    if not append_header:
        file_cvs.write((''.join(map(lambda x: 'site' in x and '"%s","tic","yaca",'%x or '"%s",'%x, json.loads(line).iterkeys()))+'\n').upper())
    append_header = True
    cvs_line = ''
    for k,v in json.loads(line).iteritems():
        try:
            cvs_line += 'site' in k and u'Нет' in v and '"%s","","",' %chr(10).join(v) or '"%s",' %chr(10).join(v).replace('"','')
        except TypeError:
            cvs_line += '"%s",' %chr(10).join(map(lambda x: x.get('url'), v))
            cvs_line += '"%s",' %chr(10).join(map(lambda x: str(x.get('tic')), v))
            cvs_line += '"%s",' %chr(10).join(map(lambda x: str(int(x.get('yaca'))), v))

    file_cvs.write(cvs_line.encode('utf-8')+'\n')
print '.done'   