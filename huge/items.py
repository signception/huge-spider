# coding: utf-8
from scrapy.item import Item, Field
from tic import get_yandex_param
import re

def add_yandex_param(site):
    site = list(set(site))
    if u'Нет' in site:
        return site
    sites_with_tic = []
    for one in site:
        one = re.search('[\w\S]+', one).group() 
        sites_with_tic.append(get_yandex_param(one))
    return sites_with_tic


class ParseItem(Item):
    # define the fields for your item here like:
    category = Field()
    page = Field()
    url = Field()
    stars = Field()
    phones = Field(input_processor=lambda x: list(set(x)))
    site = Field(input_processor=add_yandex_param)
    title = Field()
    desc = Field()
    pass
