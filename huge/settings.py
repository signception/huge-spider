# Scrapy settings for tutorial project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'huge_parser'

SPIDER_MODULES = ['huge.spiders']
NEWSPIDER_MODULE = 'huge.spiders'
LOG_LEVEL = 'INFO'


ITEM_PIPELINES = [
    'huge.pipelines.JSONPipeline',
]

USER_AGENT = 'Mozilla/5.0 (Ubuntu; X11; Linux x86_64; rv:9.0.1) Gecko/20100101 Firefox/9.0.1 FirePHP/0.6'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'tutorial (+http://www.yourdomain.com)'
