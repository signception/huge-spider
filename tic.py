import urllib
import re


def get_yandex_param(URL):
    try:
        yurl = 'http://bar-navig.yandex.ru/u?ver=2&show=32&url=http://%s' % URL.replace('http://','').encode('idna')
        
        ybody = urllib.urlopen(yurl).read()
        tic = re.search(r'value="([0-9]{1,5})"', ybody)
        yaca = re.search(r'<textinfo>(?P<author>[\W\w]+)</textinfo>', ybody)
        return {'url': URL, 'tic': tic and tic.group(1) or 0, 'yaca': yaca and (len(yaca.group(1))>2 or False) or False}
    except IOError:
        return {'url': URL, 'tic': 'http404', 'yaca': 'http404'}
    except:
        return {'url': URL, 'tic': 'unknown', 'yaca': 'unknown'}